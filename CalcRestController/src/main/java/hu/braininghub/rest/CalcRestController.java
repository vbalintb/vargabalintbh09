package hu.braininghub.rest;

import hu.braininghub.dto.CalcRequest;
import hu.braininghub.dto.CalcResponse;
import hu.braininghub.service.CalcService;
import hu.braininghub.service.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("calc_service")
public class CalcRestController {

    @Autowired
    private CalcService calcService;

    @RequestMapping(method = RequestMethod.GET, path = "hello")
    public String hello() {
        return "Hello";
    }

    @RequestMapping(method = RequestMethod.POST, path = "filter")
    public CalcResponse filterFares(@RequestBody CalcRequest calcRequest) {
        CalcResponse calcResponse = new CalcResponse();
        calcResponse.setResult(String.valueOf(calcService.calculator(Operation.valueOf(calcRequest.getOperation()), Integer.parseInt(calcRequest.getNum1()), Integer.parseInt(calcRequest.getNum2()))));
        return calcResponse;
    }
}
