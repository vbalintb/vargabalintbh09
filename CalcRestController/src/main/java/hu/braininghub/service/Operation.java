package hu.braininghub.service;

public enum Operation {
    PLUS, MINUS, TIMES, DIVIDE
}
