package hu.braininghub.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalcService {

    @Autowired
    public CalcService() {
    }

    public int calculator(Operation operation, int a, int b) {
        switch (operation) {
            case PLUS: return a+b;
            case MINUS: return a-b;
            case TIMES: return a*b;
            case DIVIDE: return a/b;
            default: return 0;
        }
    }

}
