package hu.braininghub.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class CalcRequest {

    @JsonProperty("operation")
    private String operation;

    @JsonProperty("num1")
    private String num1;

    @JsonProperty("num2")
    private String num2;

    public String getNum1() {
        return num1;
    }

    public void setNum1(String num1) {
        this.num1 = num1;
    }

    public String getNum2() {
        return num2;
    }

    public void setNum2(String num2) {
        this.num2 = num2;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalcRequest that = (CalcRequest) o;
        return Objects.equals(operation, that.operation) &&
                Objects.equals(num1, that.num1) &&
                Objects.equals(num2, that.num2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(operation, num1, num2);
    }

    @Override
    public String toString() {
        return "CalcRequest{" +
                "operation='" + operation + '\'' +
                ", num1='" + num1 + '\'' +
                ", num2='" + num2 + '\'' +
                '}';
    }
}
