package hu.braininghub.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class CalcResponse {

    @JsonProperty("result")
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalcResponse that = (CalcResponse) o;
        return Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result);
    }

    @Override
    public String toString() {
        return "CalcResponse{" +
                "result='" + result + '\'' +
                '}';
    }
}
