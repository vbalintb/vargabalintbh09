/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.ikearest;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author vbali
 */
@Configuration
@EnableJpaRepositories(basePackages = "hu.braininghub.ikearest.repository")
@ComponentScan(basePackages = "hu.braininghub.ikearest")
@EntityScan(basePackages = "hu.braininghub.ikearest.repository.entity")
public class Config {
    
}
