/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.ikearest.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author vbali
 */
@Entity
@Table(name = "ikea")
@Data
public class Products {
    @Id
    private String name;

    @Column(name="amount")
    private int amount;
}
