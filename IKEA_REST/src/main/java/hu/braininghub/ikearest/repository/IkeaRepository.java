/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.ikearest.repository;

import hu.braininghub.ikearest.repository.entity.Products;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author vbali
 */
@Repository
public interface IkeaRepository extends JpaRepository<Products, String> {
    
    public List<Products> findByAmountGreaterThan(int amount);

}
