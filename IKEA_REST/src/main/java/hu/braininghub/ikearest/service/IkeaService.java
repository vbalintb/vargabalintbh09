/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.ikearest.service;

import hu.braininghub.ikearest.dto.ProductDTO;
import hu.braininghub.ikearest.repository.IkeaRepository;
import hu.braininghub.ikearest.repository.entity.Products;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author vbali
 */
@Service
public class IkeaService {
    
    private IkeaRepository ikeaRepository;

    @Autowired
    public IkeaService(IkeaRepository ikeaRepository) {
        this.ikeaRepository = ikeaRepository;
    }

    public List<ProductDTO> filterProducts() {
        List<ProductDTO> productDTOs = new ArrayList<>();
        List<Products> products = ikeaRepository.findByAmountGreaterThan(5);
        products.forEach(item -> {
            ProductDTO dto = new ProductDTO();
            BeanUtils.copyProperties(item, dto);
            productDTOs.add(dto);
        });

        return productDTOs;
    }
}
