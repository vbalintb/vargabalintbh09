/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.ikearest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author vbali
 */
@Data
public class IkeaFilterResponse {
    @JsonProperty("products")
    private List<ProductDTO> fares = new ArrayList<>();

}
