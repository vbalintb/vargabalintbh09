/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.ikearest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 *
 * @author vbali
 */
@Data
public class ProductDTO {
    
    @JsonProperty("name")
    private String name;
    
    @JsonProperty("amount")
    private int amount;
}
