/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.ikearest.rest;

import hu.braininghub.ikearest.dto.IkeaFilterResponse;
import hu.braininghub.ikearest.service.IkeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vbali
 */
@RestController
@RequestMapping("ikea_service")
public class IkeaRestController {
    
    @Autowired
    private IkeaService ikeaService;
    
    @RequestMapping(method = RequestMethod.GET, path = "hello")
    public String hello() {
        return "Hello";
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "filter")
    public ResponseEntity filterProducts() {
        IkeaFilterResponse ikeaFilterResponse = new IkeaFilterResponse();
        ikeaFilterResponse.setFares(ikeaService.filterProducts());
        return ResponseEntity.ok(ikeaFilterResponse);

    }
}
