/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.ikearest.service;

import hu.braininghub.ikearest.dto.ProductDTO;
import hu.braininghub.ikearest.repository.IkeaRepository;
import hu.braininghub.ikearest.repository.entity.Products;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author vbali
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = IkeaServiceTest.Config.class)
public class IkeaServiceTest {
    
    @Autowired
    IkeaService ikeaService;
    @Autowired
    IkeaRepository ikeaRepository;
    
    @TestConfiguration
    @ComponentScan(basePackageClasses = IkeaService.class)
    static class Config {
        @Bean
        public IkeaRepository fareRepository(){
            return mock(IkeaRepository.class);
        }
    }

    @Test
    public void filterProductsTest() {
        List<ProductDTO> expectedResult = new ArrayList<>();
        ProductDTO productDTO1 = new ProductDTO();
        productDTO1.setName("IVAR2");
        productDTO1.setAmount(12);
        expectedResult.add(productDTO1);
        ProductDTO productDTO2 = new ProductDTO();
        productDTO2.setName("LACK2");
        productDTO2.setAmount(6);
        expectedResult.add(productDTO2);
        ProductDTO productDTO3 = new ProductDTO();
        productDTO3.setName("PAX01");
        productDTO3.setAmount(22);
        expectedResult.add(productDTO3);
        
        List<Products> mockedResult = new ArrayList<>();
        Products products1 = new Products();
        products1.setName("IVAR2");
        products1.setAmount(12);
        mockedResult.add(products1);
        Products products2 = new Products();
        products2.setName("LACK2");
        products2.setAmount(6);
        mockedResult.add(products2);
        Products products3 = new Products();
        products3.setName("PAX01");
        products3.setAmount(22);
        mockedResult.add(products3);

        when(ikeaRepository.findByAmountGreaterThan(5))
                .thenReturn(mockedResult);
        List<ProductDTO> actualResult = ikeaService.filterProducts();

        Assertions.assertEquals(expectedResult, actualResult);
    }
    
}
